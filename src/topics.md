# What That!?

- [Starting in IT](./startingIT/chapter_1.md)
- [Cybersecurity](./cybersecurity/chapter_1.md)
- [DevOps](./devops/chapter_1.md)
- [Project Ideas](./project_scopes/chapter_1.md)
